export enum FleetManagerPageName {
  HOME = 'Home',
  CASES = 'Cases',
  VEHICLES = 'Vehicles',
  DRIVERS = 'Drivers',
  ACTIVITY = 'Activity',
  REPORTS = 'Reports',
  ADMIN = 'Admin',
  LOGOUT = 'Logout'
}
