export interface IDriverTableData {
  avatar: JSX.Element
  driverName: string
  driverPhone: string
  driverEmail: string
  status: JSX.Element
  action: JSX.Element
}
