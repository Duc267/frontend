export interface IUrgentCaseTableData {
  flag: JSX.Element
  caseId: string
  regNumber: string
  dateOfIncident: string
  timeOfIncident: string
  driverName: string
  onClick: () => void
}
