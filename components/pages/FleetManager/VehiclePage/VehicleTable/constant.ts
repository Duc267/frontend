export interface IVehicleTableData {
  registrationNumber?: string
  make?: string
  model?: string
  yearOfManufacture?: number
  vinNumber?: string
  status?: JSX.Element
  dashCam: JSX.Element
  action: JSX.Element
}
